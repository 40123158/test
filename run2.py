import hashlib

# convert user_password into sha1 encoded string
def gen_password(user_password):
    return hashlib.sha1(user_password.encode("utf-8")).hexdigest()

test = 'abc%03d'
for i in range(1, 399+1):
    print(test%(i)+":"+gen_password(test%(i))+":"+test%(i)+":"+test%(i)+"@gmail.com"+":"+"admin,user")